// SPDX-License-Identifier: GPL-2.0

#include "common.h"
#include "work.h"

void main_loop()
{
    uint16_t colour;
    point_t text_pos;
    while(1)
    {
        if (interrupt == BUTTON1)
        {
            show_qr(qrcode, qrcode_len, "Contact Info");
            interrupt = INTERRUPT_RESET;
        }
        else if (interrupt == BUTTON2)
        {
            show_qr(picoqr, picoqr_len, "Project Page");
            interrupt = INTERRUPT_RESET;
        }
        colour_pattern();
        ssd1351_refresh();
        sleep_loop();
        BUTTON_CONTINUE();
        ssd1351_setup_scroll_buffered();
        ssd1351_load_image(BUFFER_TYPE_SCROLL, linuxjedi, (point_t){0, 0}, (point_t){128, 128});
        scroll_loop(SCROLL_RIGHT);
        sleep_loop();
        BUTTON_CONTINUE();
        ssd1351_setup_scroll_buffered();
        ssd1351_load_image(BUFFER_TYPE_SCROLL, linuxjedi_name, (point_t){0, 45}, (point_t){128, 38});
        scroll_loop(SCROLL_LEFT);
        sleep_loop();
        BUTTON_CONTINUE();
        for (uint8_t i = 2; i < 128; i+=2)
        {
            ssd1351_clear();
            uint8_t *buff = ssd1351_resize_image(mariadb, (point_t){128,128}, (point_t){i, i});
            ssd1351_load_image(BUFFER_TYPE_DISPLAY, buff, (point_t){64 - (i/2), 64 - (i/2)}, (point_t){i, i});
            ssd1351_refresh();
            BUTTON_BREAK();
        }
        ssd1351_load_image(BUFFER_TYPE_DISPLAY, mariadb, (point_t){0, 0}, (point_t){128, 128});
        ssd1351_refresh();
        sleep_loop();
        fade_out();
        ssd1351_clear();
        BUTTON_CONTINUE();
//        ssd1351_load_image(BUFFER_TYPE_DISPLAY, cf_logo, (point_t){0, 18}, (point_t){128, 69});
//        colour = ssd1351_make_colour(0x06, 0xf2, 0xf2);
//        ssd1351_write_text(BUFFER_TYPE_DISPLAY, "CLOUDFEST", (point_t){10, 94}, colour, FONT_LARGE, true, false);
//        ssd1351_refresh();
//        fade_in();
//        sleep_loop();
//        BUTTON_CONTINUE();
//        ssd1351_clear();
        colour = ssd1351_make_colour(0xe9, 0xae, 0x4f);
        text_pos = (point_t){28, 42};
        ssd1351_write_text(BUFFER_TYPE_DISPLAY, "Andrew", text_pos, colour, FONT_LARGE, true, false);
        text_pos = (point_t){20, 60};
        ssd1351_write_text(BUFFER_TYPE_DISPLAY, "(LinuxJedi)", text_pos, colour, FONT_TOPAZ, true, false);
        text_pos = (point_t){10, 70};
        ssd1351_write_text(BUFFER_TYPE_DISPLAY, "Hutchings", text_pos, colour, FONT_LARGE, true, false);
        ssd1351_refresh();
        fade_in();
        sleep_loop();
        BUTTON_CONTINUE();
        ssd1351_setup_scroll_buffered();
        colour = ssd1351_make_colour(0xff, 0xff, 0xff);
        text_pos = (point_t){26, 42};
        ssd1351_write_text(BUFFER_TYPE_SCROLL, "Chief", text_pos, colour, FONT_LARGE2, true, false);
        text_pos = (point_t){12, 60};
        ssd1351_write_text(BUFFER_TYPE_SCROLL, "Contributions", text_pos, colour, FONT_IBM, true, false);
        text_pos = (point_t){11, 70};
        ssd1351_write_text(BUFFER_TYPE_SCROLL, "Officer", text_pos, colour, FONT_LARGE2, true, false);
        scroll_loop(SCROLL_DOWN);
        sleep_loop();
        BUTTON_CONTINUE();
        ssd1351_setup_scroll_buffered();
        colour = ssd1351_make_colour(0x4e, 0x62, 0x9a);
        text_pos = (point_t){11, 47};
        ssd1351_write_text(BUFFER_TYPE_SCROLL, "MariaDB", text_pos, colour, FONT_LARGE2, true, false);
        text_pos = (point_t){24, 65};
        colour = ssd1351_make_colour(0xc6, 0x99, 0x7c);
        ssd1351_write_text(BUFFER_TYPE_SCROLL, "Foundation", text_pos, colour, FONT_IBM, true, false);
        scroll_loop(SCROLL_UP);
        sleep_loop();
        BUTTON_CONTINUE();
        ssd1351_setup_scroll_buffered();
        colour = ssd1351_make_colour(0xff, 0xff, 0xff);
        text_pos = (point_t){11, 47};
        ssd1351_write_text(BUFFER_TYPE_SCROLL, "andrew@", text_pos, colour, FONT_LARGE2, true, false);
        text_pos = (point_t){20, 65};
        ssd1351_write_text(BUFFER_TYPE_SCROLL, "mariadb.org", text_pos, colour, FONT_IBM, true, false);
        scroll_loop(SCROLL_DOWN);
        sleep_loop();
    }
}

