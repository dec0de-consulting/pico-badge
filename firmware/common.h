// SPDX-License-Identifier: GPL-2.0

#pragma once

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "pico/stdlib.h"
#include "gfx/ssd1351.h"
#include "images/linuxjedi.h"
#include "images/linuxjedi_name.h"
#include "images/pico_qr.h"

#define BUTTON1 0
#define BUTTON2 11

#define INTERRUPT_RESET 255
extern uint interrupt;
#define BUTTON_BREAK()    if (interrupt != INTERRUPT_RESET) break;
#define BUTTON_CONTINUE() if (interrupt != INTERRUPT_RESET) continue;
#define BUTTON_RETURN()   if (interrupt != INTERRUPT_RESET) return;

void show_qr(const uint8_t* img, const unsigned int img_len, const char *text);
void scroll_loop(scroll_direction direction);
void sleep_loop();
void fade_out();
void fade_in();
void line_test();
void colour_pattern();
