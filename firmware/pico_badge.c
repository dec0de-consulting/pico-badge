// SPDX-License-Identifier: GPL-2.0

#include "common.h"

#include "amiga.h"
#include "work.h"

const uint LED_PIN = 25;
uint interrupt = INTERRUPT_RESET;
void gpio_callback(uint gpio, uint32_t events)
{
    interrupt = gpio;
}

int main() {
    bool loop_main = true;
    stdio_init_all();
    bi_decl(bi_1pin_with_name(LED_PIN, "On-board LED"));

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    gpio_put(LED_PIN, 1);
    ssd1351_init();
    gpio_put(LED_PIN, 0);

    gpio_init(BUTTON1);
    gpio_set_dir(BUTTON1, GPIO_IN);
    gpio_pull_up(BUTTON1);
    gpio_init(BUTTON2);
    gpio_pull_up(BUTTON2);
    gpio_set_dir(BUTTON2, GPIO_IN);
    if (gpio_get(BUTTON1) == 0)
    {
        loop_main = false;
    }
    gpio_set_irq_enabled_with_callback(BUTTON1, GPIO_IRQ_EDGE_FALL, true, &gpio_callback);
    gpio_set_irq_enabled(BUTTON2, GPIO_IRQ_EDGE_FALL, true);
    if (loop_main)
    {
        main_loop();
    }
    else
    {
        amiga_loop();
    }
    return 0;
}
