// SPDX-License-Identifier: GPL-2.0

#pragma once
#include "images/kickstart.h"
#include "images/amiga_tick.h"
#include "images/boing_ball.h"
#include "images/personal_qr.h"

void amiga_loop();
