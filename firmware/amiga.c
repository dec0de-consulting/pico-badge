// SPDX-License-Identifier: GPL-2.0

#include "common.h"
#include "amiga.h"

static void boing_ball(int total_frames)
{
    int frame = 0;
    int iter = 0;
    int xpos = 0;
    bool dir = false;
    while(1)
    {
        const char *sprite;
        switch (frame)
        {
            case 0:
                sprite = boing_frame0;
                break;
            case 1:
                sprite = boing_frame1;
                break;
            case 2:
                sprite = boing_frame2;
                break;
            case 3:
                sprite = boing_frame3;
                break;
        }
        int ypos = 64-abs((int)((float)64 * cos((float)iter * 3.6 * M_PI/180.0)));
        ssd1351_clear();
        ssd1351_load_image(BUFFER_TYPE_DISPLAY, sprite, (point_t){xpos, ypos}, (point_t){64, 64});
        ssd1351_refresh();
        sleep_ms(30);
        iter++;
        if (iter == total_frames)
        {
            return;
        }
        if (!dir)
        {
            xpos++;
            if (xpos == 63)
            {
                dir = true;
            }
            frame++;
            if (frame == 4) frame = 0;
        }
        else
        {
            xpos--;
            if (xpos == 0)
            {
                dir = false;
            }
            frame--;
            if (frame == -1) frame = 3;
        }
        BUTTON_BREAK();
    }
}

static void kickstart_ani()
{
    for (uint8_t i = 1; i <= 16; i++)
    {
        //uint16_t colour = ssd1351_make_colour(0x44, 0x11, 0x44);
        //ssd1351_fill_colour(BUFFER_TYPE_DISPLAY, colour);
        ssd1351_clear();
        ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_drive, (point_t){9, 0}, (point_t){110, 27});
        switch(i)
        {
            case 1:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk1, (point_t){16, 36}, (point_t){95, 85});
                break;
            case 2:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk1, (point_t){16, 43}, (point_t){95, 85});
                break;
            case 3:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk3, (point_t){16, 40}, (point_t){95, 79});
                break;
            case 4:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk4, (point_t){16, 37}, (point_t){95, 71});
                break;
            case 5:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk5, (point_t){16, 34}, (point_t){95, 67});
                break;
            case 6:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk6, (point_t){16, 31}, (point_t){95, 61});
                break;
            case 7:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk7, (point_t){16, 28}, (point_t){95, 55});
                break;
            case 8:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk8, (point_t){16, 26}, (point_t){95, 48});
                break;
            case 9:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk9, (point_t){16, 23}, (point_t){95, 43});
                break;
            case 10:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk10, (point_t){16, 20}, (point_t){95, 35});
                break;
            case 11:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk11, (point_t){16, 17}, (point_t){95, 30});
                break;
            case 12:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk12, (point_t){16, 14}, (point_t){95, 24});
                break;
            case 13:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk13, (point_t){16, 11}, (point_t){95, 17});
                break;
            case 14:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk14, (point_t){16, 9}, (point_t){95, 11});
                break;
            case 15:
                ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk15, (point_t){16, 6}, (point_t){95, 4});
                break;
            case 16:
                break;
        }
        ssd1351_refresh();
        sleep_ms(50);
        BUTTON_BREAK();
    }
    sleep_loop();
}

void amiga_loop()
{
    uint16_t colour;
    point_t text_pos;
    while(1)
    {
        if (interrupt == BUTTON1)
        {
            show_qr(personal_qr, 2048, "Contact Info");
            interrupt = INTERRUPT_RESET;
        }
        else if (interrupt == BUTTON2)
        {
            show_qr(picoqr, picoqr_len, "Project Page");
            interrupt = INTERRUPT_RESET;
        }
        ssd1351_clear();
        ssd1351_set_contrast(255);
        colour_pattern();
        ssd1351_refresh();
        sleep_loop();
        fade_out();
        BUTTON_CONTINUE();
        //uint16_t colour = colour = ssd1351_make_colour(0x44, 0x11, 0x44);
        //ssd1351_fill_colour(BUFFER_TYPE_DISPLAY, colour);
        ssd1351_clear();
        ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_drive, (point_t){9, 0}, (point_t){110, 27});
        ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk1, (point_t){16, 36}, (point_t){95, 85});
        ssd1351_refresh();
        fade_in();
        BUTTON_CONTINUE();
        kickstart_ani();
        ssd1351_clear();
        //ssd1351_fill_colour(BUFFER_TYPE_DISPLAY, colour);
        ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_drive, (point_t){9, 0}, (point_t){110, 27});
        ssd1351_load_image(BUFFER_TYPE_DISPLAY, kick_disk1, (point_t){16, 36}, (point_t){95, 85});
        ssd1351_refresh();
        sleep_loop();
        BUTTON_CONTINUE();
        kickstart_ani();
        BUTTON_CONTINUE();
        ssd1351_setup_scroll_buffered();
        ssd1351_load_image(BUFFER_TYPE_SCROLL, amiga_tick, (point_t){0, 5}, (point_t){128, 118});
        scroll_loop(SCROLL_RIGHT);
        sleep_loop();
        fade_out();
        BUTTON_CONTINUE();
        ssd1351_load_image(BUFFER_TYPE_DISPLAY, linuxjedi, (point_t){0, 0}, (point_t){128,128});
        ssd1351_refresh();
        fade_in();
        sleep_loop();
        BUTTON_CONTINUE();
        ssd1351_setup_scroll_buffered();
        ssd1351_load_image(BUFFER_TYPE_SCROLL, linuxjedi_name, (point_t){0, 45}, (point_t){128, 38});
        scroll_loop(SCROLL_LEFT);
        sleep_loop();
        fade_out();
        ssd1351_clear();
        colour = ssd1351_make_colour(0xe9, 0xae, 0x4f);
        text_pos = (point_t){28, 42};
        ssd1351_write_text(BUFFER_TYPE_DISPLAY, "Andrew", text_pos, colour, FONT_LARGE, true, false);
        text_pos = (point_t){20, 60};
        ssd1351_write_text(BUFFER_TYPE_DISPLAY, "(LinuxJedi)", text_pos, colour, FONT_TOPAZ, true, false);
        text_pos = (point_t){10, 70};
        ssd1351_write_text(BUFFER_TYPE_DISPLAY, "Hutchings", text_pos, colour, FONT_LARGE, true, false);
        ssd1351_refresh();
        fade_in();
        sleep_loop();
        BUTTON_CONTINUE();
        fade_out();
        ssd1351_clear();
        colour = ssd1351_make_colour(0xff, 0xff, 0xff);
        text_pos = (point_t){11, 47};
        ssd1351_write_text(BUFFER_TYPE_SCROLL, "andrew@", text_pos, colour, FONT_LARGE2, true, false);
        text_pos = (point_t){1, 65};
        ssd1351_write_text(BUFFER_TYPE_DISPLAY, "linuxjedi.co.uk", text_pos, colour, FONT_IBM, true, false);
        ssd1351_refresh();
        fade_in();
        BUTTON_CONTINUE();
        sleep_loop();
        fade_out();
        ssd1351_clear();
        ssd1351_set_contrast(255);
        boing_ball(600);
    }
}

