// SPDX-License-Identifier: GPL-2.0

#include "common.h"

void show_qr(const uint8_t* img, const unsigned int img_len, const char *text)
{
    ssd1351_clear();
    ssd1351_set_contrast(255);
    ssd1351_load_image_1bpp(BUFFER_TYPE_DISPLAY, img, img_len);
    uint16_t colour = ssd1351_make_colour(0x00, 0x00, 0x00);
    point_t text_pos = {16, 118};
    ssd1351_write_text(BUFFER_TYPE_DISPLAY, text, text_pos, colour, FONT_IBM, true, false);
    ssd1351_refresh();
    sleep_ms(10000);
    ssd1351_clear();
}

void scroll_loop(scroll_direction direction)
{
    for (uint16_t i = 0; i < 64; i++)
    {
        ssd1351_scroll_buffered(2, direction);
        ssd1351_refresh();
        BUTTON_BREAK();
    }
}

void sleep_loop()
{
    for (uint16_t i = 0; i < 20; i++)
    {
        sleep_ms(100);
        BUTTON_BREAK();
    }
}

void fade_out()
{
    for (int16_t i = 255; i >= 0; i--)
    {
        sleep_ms(1);
        ssd1351_set_contrast(i);
        BUTTON_BREAK();
    }
}

void fade_in()
{
    for (uint16_t i = 0; i <= 255; i++)
    {
        sleep_ms(1);
        ssd1351_set_contrast(i);
        BUTTON_BREAK();
    }
}


void line_test()
{
    uint16_t colour = ssd1351_make_colour(0x7f, 0x7f, 0xff);
    ssd1351_clear();
    ssd1351_draw_line(BUFFER_TYPE_DISPLAY, colour, (point_t){0, 0}, (point_t){128, 128});
    ssd1351_draw_line(BUFFER_TYPE_DISPLAY, colour, (point_t){128, 0}, (point_t){0, 128});
    ssd1351_draw_line(BUFFER_TYPE_DISPLAY, colour, (point_t){64, 0}, (point_t){64, 128});
    ssd1351_draw_line(BUFFER_TYPE_DISPLAY, colour, (point_t){0, 32}, (point_t){128, 96});
    ssd1351_draw_line(BUFFER_TYPE_DISPLAY, colour, (point_t){0, 96}, (point_t){128, 32});
    colour = ssd1351_make_colour(0x00, 0xff, 0x00);
    ssd1351_draw_rectangle(BUFFER_TYPE_DISPLAY, colour, (point_t){10, 10}, (point_t){90, 40}, false);
    ssd1351_draw_rectangle(BUFFER_TYPE_DISPLAY, colour, (point_t){90, 90}, (point_t){110, 120}, true);
    colour = ssd1351_make_colour(0xff, 0x7f, 0x7f);
    ssd1351_draw_circle(BUFFER_TYPE_DISPLAY, colour, (point_t){64, 64}, 20, true);
    colour = ssd1351_make_colour(0xff, 0xff, 0x00);
    ssd1351_draw_triangle(BUFFER_TYPE_DISPLAY, colour, (point_t){20, 70}, (point_t){30, 85}, (point_t){40, 60}, false);
    ssd1351_draw_triangle(BUFFER_TYPE_DISPLAY, colour, (point_t){80, 40}, (point_t){90, 90}, (point_t){120, 60}, true);
}

void colour_pattern()
{
    ssd1351_clear();
    point_t pos = {0, 0};
    point_t text_pos;
    uint8_t *buffer;
    for (int j = 0; j <= HEIGHT * 2; j+=4)
    {
        for (int i = 0; i < WIDTH * HEIGHT; i++)
        {
            uint16_t colour = ssd1351_make_colour((j + pos.x * 2), (j + pos.y * 2), (255 - (j + pos.x * 2)));
            ssd1351_draw_pixel(BUFFER_TYPE_DISPLAY, colour, pos);
            pos.x++;
            if (pos.x == 128)
            {
                pos.x = 0;
                pos.y++;
            }
        }
        text_pos = (point_t){20, 0};
        ssd1351_write_text(BUFFER_TYPE_DISPLAY, "LinuxJedi's", text_pos, 0, FONT_TOPAZ, true, true);
        text_pos = (point_t){4, 10};

        ssd1351_write_text(BUFFER_TYPE_DISPLAY, "OLED Pico Badge", text_pos, 0, FONT_TOPAZ, true, true);
        ssd1351_refresh();
        pos.x = 0;
        pos.y = 0;
        BUTTON_BREAK();
    }
}

