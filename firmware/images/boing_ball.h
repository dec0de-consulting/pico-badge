// SPDX-License-Identifier: GPL-2.0

#pragma once

// Images converted from https://mike42.me/blog/2022-11-porting-the-amiga-bouncing-ball-demo-to-the-nes

extern const unsigned char boing_frame0[];
extern const unsigned char boing_frame1[];
extern const unsigned char boing_frame2[];
extern const unsigned char boing_frame3[];
extern const unsigned int boing_len;
