// SPDX-License-Identifier: GPL-2.0

#pragma once

extern const unsigned char kick_drive[];
extern const unsigned char kick_disk1[];
extern const unsigned char kick_disk3[];
extern const unsigned char kick_disk4[];
extern const unsigned char kick_disk5[];
extern const unsigned char kick_disk6[];
extern const unsigned char kick_disk7[];
extern const unsigned char kick_disk8[];
extern const unsigned char kick_disk9[];
extern const unsigned char kick_disk10[];
extern const unsigned char kick_disk11[];
extern const unsigned char kick_disk12[];
extern const unsigned char kick_disk13[];
extern const unsigned char kick_disk14[];
extern const unsigned char kick_disk15[];
