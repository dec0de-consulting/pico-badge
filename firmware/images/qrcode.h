// SPDX-License-Identifier: GPL-2.0

#pragma once

extern const unsigned char qrcode[];
extern const unsigned int qrcode_len;
