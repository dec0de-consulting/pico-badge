# Pico Badge

## Introduction

This is an in-development project to make a video badge using a Raspberry Pi Pico and an SSD1351 OLED display.

It uses two switching DMA buffers so that the RP2040 can write one image to the OLED display whilst simultaneously generating the next frame.

### First prototype

![Breadboard prototype](prototype.jpg)

### Version 1.1

![Version 1.1](v1.1.jpg)

## PCB Design

PCB design files for 1.1 have been included, these require at least KiCad 7 to use. Note that in v1.1 the hole for the OLED ribbon cable is not wide enough, the edges of the ribbon (which have no traces) will need trimming.

## Wiring

To breadboard this desigh you need to wire the OLED display to the Pi using the following:

| SSD1351 pin | Pico GPIO | Pico Pin Number |
| ----------- | --------- | --------------- |
| GND         | GND       | Any of 3, 8, 13, 18, 38, 28, 23 |
| VCC         | 3V3(OUT)  | 36 |
| SCL         | 6         | 9  |
| SDA         | 7         | 10 |
| RES         | 4         | 6  |
| DC          | 5         | 7  |
| CS          | GND       | Any of 3, 8, 13, 18, 38, 28, 23 |

## Compiling

To compile the code in Linux you need to do the following:

1. Install `arm-none-eabi-gcc` and related tooling on your system (see Raspberry Pi Pico's manual for this)
2. Compile using the following commands:

```sh
git submodule update --init --recursive
cd firmware
mkdir build
cd build
cmake ..
make
```

You will then get a resulting `pico_badge.uf2` to flash.

## Running

By default this runs a loop showing my day job information. Holding down the right button on power up runs a loop showing my Amiga information.

In both modes the left button shows a QR code that links to this project page. The right button shows a contact QR code.
