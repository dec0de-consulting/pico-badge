#!/bin/bash
# SPDX-License-Identifier: GPL-2.0

die () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 1 ] || die "Usage convert.sh <filename.png>"

python img2bin.py $1 image
xxd -i image > image.h
rm image
