#!/usr/bin/python
# SPDX-License-Identifier: GPL-2.0

import sys
import numpy
from PIL import Image

def process_file(infile, outfile):
    img = Image.open(infile)

#    if ((img.size[0] != 128) or (img.size[1] != 128)):
#        print("This tool only works with 128x128 images")
#        sys.exit(1)
    pix = img.load()
    outarray = numpy.zeros((img.size[1]*img.size[0],),dtype=numpy.int16)
    array_pos = 0
    with open(outfile, "wb") as out_file:
        for y_pos in range(img.size[1]):
            for x_pos in range(img.size[0]):
                word = 0
                word = pix[x_pos,y_pos][0]>>3
                word <<= 6
                word |= pix[x_pos,y_pos][1]>>2
                word <<= 5
                word |= pix[x_pos,y_pos][2]>>3
                outarray[array_pos] = word
                array_pos = array_pos+1
        outarray.byteswap(inplace=True)
        out_file.write(outarray)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage img2bin.py <infile.png> <outfile>")
        sys.exit(1)
    process_file(sys.argv[1], sys.argv[2])
