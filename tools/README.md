# Image converter

This tool converts a PNG image to a hex representation header file that can be rendered on the SSD1351 OLED display.

## Usage

```sh
./convert.sh image.png
```

Where `image.png` is a 128x128px image file. This will generate a file called `image.h` which will contain an array called `image` with the image data and `image_len` which will contain the size of the array (always 32768 bytes).

## Manual 1bpp image conversion

```sh
convert myimage.png -depth 1 GRAY:out.bin
xxd -i out.bin > header.h
```
